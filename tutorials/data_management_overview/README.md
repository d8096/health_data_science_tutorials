

This tutorial is dedicated to the data management of health data. It present a summary of the main data management operations, organized into a workflow.

The tutorial is written in [Markdown mark up language](https://www.markdownguide.org/).

The first version contains the main R functions required to data manage csv files.

# Data management workflow

<center>
	<img src="images/data_management_process.gif">
</center>

# Using the tutorial

## Download this repository

You can do this with either git clone https://gitlab.com/d8096/health_data_science_tutorials at the command line or by downloading this repostiory as a Zip file.

## Open Jupyter Lab 

Open the Notebook of your choice. All paths are relative paths.

# Contributing

Feel free to submit an issue or to launch a pull request.
