

This tutorial is dedicated to the sankey diagram. It present a step-by-step data management process to easily construct Sankey diagrams.

Two versions of the tutorial are avaible :

- the first one is a Jupyter Notebook written in [Markdown mark up language](https://www.markdownguide.org/). 
- the second one is a R script which can be executed in R studio. 

# Example of Sankey Diagram

<center>
	<img src="images/sankey_diagram.png">
</center>

# Requirements

- Notebook editor or Rstudio
- R packages : dplyr, networkD3 

# Data management workflow and building of the diagram

- Step 1 : Sorting and ranking records chronologically
- Step 2 : From a step-oriented table to a transition-oriented table
- Step 3 : From an indiviual-centered table to a flow-centered table
- Step 4: Graphic function

# Using the tutorial

## Download the entire directory

1. You can do this with either git clone https://gitlab.com/d8096/health_data_science_tutorials at the command line or by downloading this repository as a Zip file.

2. You can also download the dataset and script required for this tutorial :

- download the folder data
- download the folder image
- download the R script (tutorial_sankey_diagram_part_1.R) or the notebook (tutorial_sankey_diagram_part_1.ipynb)
- be careful to update relative path to folders data and image if necessary

## Open you code editor

Open the R script in Rstudio or the notebook in the Jupyter Notebook. 

## Execution

- all paths are relative paths. Make attention to the folder in which you download the data.
- execute instructions one after the other. 

# Contributing

Feel free to submit an issue or to launch a pull request.
