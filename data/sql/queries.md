# Outils

| Outil | Description | Lien |
| -------- | -------- | -------- |
| SQL Online |Plusieurs SGBD disponibles en ligne : SQLite, MariaDB, PostgreSQL, MS SQL, Oracle | https://sqliteonline.com |
| PostgreSQL | | https://www.postgresql.org/download/ |
| DBeaver | Interface graphique pour PostgreSQL et autres SGBD| https://www.postgresql.org/download/ |

# Interrogation

```sql
SELECT expr [, expr ...]
FROM table
WHERE condition 
[ GROUP BY expression ] 
ORDER BY expression
```

### Sélectionner toutes les colonnes et toutes les lignes de la table patient

```sql
select * 
from patient;
```

<p style="color:red"><b>Q1 - Sélectionner toutes les lignes de la table <i>hospital_stay</i>.</b></p>

### Sélectionner des colonnes

Sélectionner toutes les colonnes.
```sql
select *
from patient;
```

Sélectionner seulement les colonnes *patient_id* et *birth_date*.
```sql
select patient_id, birth_date
from patient;
```

Compter le nombre de lignes.
```sql
select count(*)
from patient;
```

Renommer une colonne à l'affichage.
```sql
select count(*) as nb_patients
from patient;
```

<p style="color:#21618c";><b>Q2 - Sélectionner les colonnes <i>patient_id</i> et <i>sex</i> dans la table <i>patient</i>.</b></p>

<p style="color:#21618c";><b>Q3 - Compter le nombre de lignes de la table <i>hospital_stay</i> et renommer la valeur <i>nb_sejours</i>.</b></p>

### Trier les lignes

#### Par ordre croissant

```sql
select *
from patient
order by birth_date;
```

#### Par ordre décroissant

```sql
select *
from patient
order by birth_date desc;
```

<p style="color:#21618c";><b>Q4 - Trier les lignes de la table <i>hospital_stay</i> par ordre décroissant de date de fin de séjour (<i>end_date</i>).</b></p>

### Filtrer les lignes

```sql
select *
from patient
where sex = 'M';
```

```sql
select *
from patient
where sex = 'M'
and location_id = 1;
```

<p style="color:#21618c";><b>Q5 - Filtrer les lignes de la table <i>hospital_stay</i> avec <i>start_date</i> après le 01/02/2020.</b></p>

### Regrouper des lignes

```sql 
select sex, count(*) as nb_patients
from patient
group by sex;
```
<p style="color:#21618c";><b>Q6 - Regrouper les lignes de la table <i>patient</i> par <i>location_id</i> et renommer le nombre de lignes par <i>nb_patients</i>.</b></p>

### Associer plusieurs tableaux

Si N tables impliquées dans la jointure, N-1 clause de jointure.
Ici patient et hospital_stay -> patient.patient_id = hospital_stay.patient_id

```sql 
select *
from patient, hospital_stay
where patient.patient_id = hospital_stay.patient_id;
```

<p style="color:#21618c";><b>Q7 - Associer les tables <i>patient</i> et <i>location</i>.</b></p>

### Exercices

<p style="color:#21618c";><b>Q8 - Compter le nombre de patients par ville de naissance (en affichant le libellé de la ville). <i>patient</i> et <i>location</i>.</b></p>

<p style="color:#21618c";><b>Q9 - Sélectionner les patients ayant plus de 1 séjour. <i>patient</i> et <i>location</i>.</b></p>